# Orkun <> Candy First Project

This project is based on Arduino and BT Serial Monitor, it can catch the position and send to your phone. For example, you set it on the door so someone open the door you can know it.
Arduino MPU6050 6 axis accelerometer+gyroscope + BT Serial Monitor project

## Goal
To receive MPU6050 readouts on an android device, seeing the changes in dimensions

## Simple Steps
* step 1: CONNECT ARDUINO, MPU6050, AND BT HC05 BT MODULE TOGETHER
HC05    MPU6050     ARDUINO
RX                  TX
TX.                 RX
GND.    GND.        GND
VCC.    vcc.        5V
        scl.        A5
        sda.        A4
        int.        D2      
![](https://gitlab.com/candypic/candyprojectpic/uploads/b79738207e219ef9c1aaa505184deb40/20200820195425.jpeg)
* step 2: upload the code
* step 3: set the baud rate to 9600 in serial monitor and the phone bluetooth terminal software
* step 4: pair phone with bluetooth module and start receiving Tx
## Questions and Solved
* Q1: Can't Find the Port
* Q2: The phone can't show the data on the phone

Below is the short video of project working.:
![](https://gitlab.com/candypic/candyprojectpic/uploads/a8a18a33b32587383c0f69f09143d6cf/20200820201611.gif)