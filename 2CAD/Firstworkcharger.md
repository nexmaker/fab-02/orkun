Project Goal: To copy the dimensional parameters of a charger body on CAD and convert and optimize the drawings for 3D Printing
Steps: Create body of the device
![Device Body](2CAD/devicebody.png)
Create the remaining parts
![Devicebody-with-auxillaries](2CAD/devicebody-with-auxillaries.png)